var path = require('path');

module.exports = {
    entry: "./app/app.js",
    output: {
        filename: "build/dist/bundle.js",
        sourceMapFilename: "build/dist/bundle.map"
    },
    devtool: '#source-map',
    module: {
        loaders: [
            {
                loader: 'babel',
                exclude: /node_modules/
            }
        ]
    }
}
