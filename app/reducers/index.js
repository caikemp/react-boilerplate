import { combineReducers } from 'redux';

// Reducers
import testReducer from './test-reducer';


// Combine Reducers
var reducers = combineReducers({
    testState: testReducer
});

export default reducers;
