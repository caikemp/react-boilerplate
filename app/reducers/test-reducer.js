import * as types from '../actions/action-types';
import _ from 'lodash';

const initialState = {
  tests: []
};

const testReducer = function(state = initialState, action) {

  switch(action.type) {

    case types.GET_WIDGETS_SUCCESS:
      return Object.assign({}, state, { tests: action.tests });

    case types.DELETE_WIDGET_SUCCESS:

      // Use lodash to create a new widget array without the widget we want to remove
      const newWidgets = _.filter(state.tests, widget => widget.id != action.widgetId);
      return Object.assign({}, state, { tests: newWidgets })

  }

  return state;

}

export default testReducer;
