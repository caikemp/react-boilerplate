import store from '../store';
import api from '../api.config';
import { getWidgetsSuccess, deleteWidgetSuccess } from '../actions/test-actions';
// import { normalize, Schema, arrayOf } from 'normalizr';

// const calendar = new Schema('titulo');
// const game = new Schema('listajogos');



/**
 * Get widgets
 */
 let unsubscribe = store.subscribe(() =>
   console.log(store.getState())
 )
export function getWidgets() {
  return api.get('/post.php', {
    teste: 'te',
    id: '1'
  }).then(response => {
      // calendar.define({
      //   calendar: calendar,
      //   games: arrayOf(game, { schemaAttribute: 'jogo' })
      // });
      // console.log(calendar);
      // teste = normalize(response, calendar);
      // console.log('getWidgets', teste);
      store.dispatch(getWidgetsSuccess(response.data.posts));
      unsubscribe();
      return response;
    }).catch(function (response){
      // console.log('error', response);
    });
}

/**
 * Search Widgets
 */

export function searchWidgets(query = '') {
  return api.get('/widgets?q='+ query)
    .then(response => {
      tests = {'tests': response.data};
      store.dispatch(getWidgetsSuccess(tests));
      return response;
    });
}

/**
 * Delete a widget
 */

export function deleteWidget(widgetId) {
  return api.delete('/widgets/' + widgetId)
    .then(response => {
      store.dispatch(deleteWidgetSuccess(widgetId));
      unsubscribe();
      return response;
    });
}
