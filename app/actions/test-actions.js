import * as types from '../actions/action-types';

export function getWidgetsSuccess(tests) {
  return {
    type: types.GET_WIDGETS_SUCCESS,
    tests
  };
}

export function deleteWidgetSuccess(widgetId) {
  return {
    type: types.DELETE_WIDGET_SUCCESS,
    widgetId
  };
}
