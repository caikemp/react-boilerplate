import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://jogorama.com.br/api/v1.0',
});

instance.interceptors.response.use(function (response) {
    // Do something with response data
    // console.log('ok',response);
    return response;
  }, function (error) {
    // Do something with response error
    console.log('error', error);
    return Promise.reject(error);
  });

export default instance;
