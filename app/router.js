import React from 'react';
import { Router, Route, browserHistory, IndexRoute } from 'react-router';

// Layouts
import MainLayout from './components/layouts/main-layout';
import SearchLayout from './components/layouts/search-layout';

// Pages
import Home from './components/views/home';
import UserList from './components/views/user-list';
import UserProfile from './components/views/user-profile';
import WidgetList from './components/views/widget-list';
import VisibleTestList from './components/containers/VisibleTestList'

const AppRouter = () => (
  <Router history={browserHistory}>
    <Route component={MainLayout}>
      <Route path="/" component={Home} />

      <Route path="users">
        <Route component={SearchLayout}>
          <IndexRoute component={UserList} />
        </Route>
        <Route path=":userId" component={UserProfile} />
      </Route>

      <Route path="widgets">
        <Route component={SearchLayout}>
          <IndexRoute component={VisibleTestList} />
        </Route>
      </Route>

    </Route>
  </Router>
);

export default AppRouter
