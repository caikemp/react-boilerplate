import React, { PropTypes } from 'react';
import Test from './test';

const WidgetList = ({ tests }) => (
  <ul>
    {tests.map(test =>
      <Test
        key={test.id}
        {...test}
      />
    )}
  </ul>
);

WidgetList.propTypes = {
  tests: PropTypes.arrayOf(PropTypes.shape({
    titulo: PropTypes.string.isRequired
  }))
}


export default WidgetList;
