import React, { PropTypes } from 'react';

const Test = ({ titulo }) => (
      <li>
        {titulo}
      </li>
    )

Test.propTypes = {
  titulo: PropTypes.string.isRequired
}

export default Test
