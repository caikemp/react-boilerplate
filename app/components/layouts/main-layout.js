import React from 'react';
import { Link } from 'react-router';

import * as testApi from '../../services/test.service';

const MainLayout = React.createClass({
  render: function() {
    return (
      <div className="app">
        <header className="primary-header"></header>
        <aside className="primary-aside">
          <ul>
            <li><Link to="/" activeClassName="active">Home</Link></li>
            <li>
              <Link to="/users" activeClassName="active">Users</Link>
              <button onClick={testApi.getWidgets} className="delete">Teste</button>
            </li>
            <li><Link to="widgets" activeClassName="active">Widgets</Link></li>
          </ul>
        </aside>
        <main>
          {this.props.children}
        </main>
      </div>
    );
  }
});

export default MainLayout;
