import { connect } from 'react-redux'
import TestList from '../views/widget-list'


const mapStateToProps = (state) => {
  return {
    tests: state.testState.tests
  }
}

const VisibleTestList = connect(
  mapStateToProps
)(TestList);

export default VisibleTestList;
