import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';

// Notice that we've organized all of our routes into a separate file.
import AppRouter from './router';

import store from './store';

// Now we can attach the router to the 'root' element like this:
ReactDOM.render(
  <Provider store={store}>
    <AppRouter />
  </Provider>
  , document.getElementById('root'));
